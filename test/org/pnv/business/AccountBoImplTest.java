package org.pnv.business;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.pnv.dao.AccountDao;
import org.pnv.model.Account;


@RunWith(MockitoJUnitRunner.class)
public class AccountBoImplTest {
	@Mock
	private AccountDao accountDao;
	@InjectMocks
	private AccountBoImpl accountBoImpl;
	
	
	@Test 
	public void shouldReturn1WhenUserAndPasswordCorrect(){
		Mockito.when(accountDao.selectDataToCheckLogin(Mockito.anyString(), Mockito.anyString())).thenReturn(1);
		int expectedResult = 1;
		int actualResult = accountDao.selectDataToCheckLogin("kgreo", "klgot");
		Assertions.assertThat(actualResult).isEqualTo(expectedResult);
	}
	
	@Test 
	public void shouldReturn0WhenUserAndPasswordCorrectAndIsactive0(){
		Mockito.when(accountDao.selectDataToCheckLogin(Mockito.anyString(), Mockito.anyString())).thenReturn(0);
		int expectedResult = 0;
		int actualResult = accountDao.selectDataToCheckLogin("kgreo", "klgot");
		Assertions.assertThat(actualResult).isEqualTo(expectedResult);
	}
	
	@Test 
	public void shouldReturnNegative1WhenUserAndPasswordIncorrect(){
		Mockito.when(accountDao.selectDataToCheckLogin(Mockito.anyString(), Mockito.anyString())).thenReturn(-1);
		int expectedResult = -1;
		int actualResult = accountDao.selectDataToCheckLogin("kgreo", "klgot");
		Assertions.assertThat(actualResult).isEqualTo(expectedResult);
	}
	
	

	
}
