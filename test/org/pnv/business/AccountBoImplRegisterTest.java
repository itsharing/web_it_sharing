package org.pnv.business;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.pnv.dao.AccountDao;
import org.pnv.model.Account;
import org.pnv.util.Utils;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@PrepareForTest(Utils.class)
@RunWith(PowerMockRunner.class)
public class AccountBoImplRegisterTest {
	
	@Mock
	private AccountDao accountDao;
	@InjectMocks
	private AccountBoImpl accountBoImpl;
	
	@Test
	public void shouldReturnTrueWhenRegisterSuccessful(){
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.when(Utils.verifyEmailToActivedAccount(Mockito.anyString(), Mockito.anyString())).thenReturn("123456");
		Account account = new Account();
		account.setVerifyCode("123456");
		Mockito.when(accountDao.insertNewAccount(Mockito.anyObject())).thenReturn(true);		
		boolean expectResult = true;		
		boolean actualResult = accountDao.insertNewAccount(account);		
		Assertions.assertThat(expectResult).isEqualTo(actualResult);
	}
	
	@Test 
	public void shouldReturnFalseWhenRegisterFail(){
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.when(Utils.verifyEmailToActivedAccount(Mockito.anyString(), Mockito.anyString())).thenReturn("123456");
		Account account = new Account();
		account.setVerifyCode("123456");
		Mockito.when(accountDao.insertNewAccount(Mockito.anyObject())).thenReturn(false);		
		boolean expectResult = false;		
		boolean actualResult = accountDao.insertNewAccount(account);		
		Assertions.assertThat(expectResult).isEqualTo(actualResult);
	}
	@Test
	public void shouldReturnFalseWhenUserNotExistAndReturnfalse(){
		Mockito.when(accountDao.selectCode(Mockito.anyString())).thenReturn("0");
		boolean expectedResult = false;
		boolean actualResult = accountBoImpl.checkVerifyCode("12321", "sang@gmail.com");
		Assertions.assertThat(actualResult).isEqualTo(expectedResult);
	}
	@Test
	public void shouldReturnTrueWhenUserExist(){
		Mockito.when(accountDao.selectEmail("sangtran")).thenReturn(true);
		boolean expectedResult = true;
		boolean actualResult = accountBoImpl.isExistEmail("sangtran");
		Assertions.assertThat(actualResult).isEqualTo(expectedResult);
	}
	
	@Test 
	public void shouldReturnFalseWhenExistEmail(){
		Mockito.when(accountDao.selectEmail(Mockito.anyString())).thenReturn(false);
		boolean expectOutput = false;
		boolean actualResult = accountDao.selectEmail("abc@gmail.com");
		Assertions.assertThat(expectOutput).isEqualTo(actualResult);
	}
	
	
}
