<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Welcome to IT sharing website</title>
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
      <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="./assets/bootstrap/css/font-awesome.min.css">
      <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
      <style type="text/css">
         .form-group-pass {
         float: right;
         position: relative;
         top: -51px;
         left: -103px;
         }
         .form-group-a {
         padding-top: 20px;
         }
         .button {
         float: right;
         position: relative;
         top: -25px;
         left: 180px;
         }
      </style>
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
            <script type="text/javascript">
               $(document).ready(function () {
                  var x_timer;
                  $("#email").keyup(function (e) {
                     clearTimeout(x_timer);
                     var email = $(this).val();
                        x_timer = setTimeout(function () {
                        check_username_ajax(email);
                     }, 1000);
                  });

               function check_username_ajax(email) {
                  $("#user-result").html('<img src="./assets/images/ajax-loader.gif" />');
                  $.post('./checkEmailServlet', {'email': email}, function (data) {
                	  $("#user-result").html(data);
                	  if (data == "<div style='color:red'>This email has been used !</div>") {
                		document.getElementById("register").disabled = true;
                	  } else {
                		document.getElementById("register").disabled = false;
                	  }
                  });
               }
            });
        </script>
   </head>
   <body style="background: #fff color:#fff;font-size:15px;">
      <div class="header" style="background:#0099FF ">
         <div class="container" style="height:120px">
            <div class="row">
               <div class="col-md-6" style="float: right;">
                  <form class="form-horizontal" role="form" action="accountServlet" method="POST">
                     <div class="form-group-a">
                        <label>Email</label><br/>
                        <input style="color:black; height:25px" type="text" placeholder="Email..." name="form-username" id="form-username" autofocus>
                     </div>
                     <div class="form-group-pass">
                        <label>Password</label><br>
                        <input style="color:black; height:25px" type="password" placeholder="Password..." name="form-password" id="form-password"> <br/>
                        <p> <a href="#" style="color: blue">Forgot Password ?</a></p>
                     </div>
                     <div class="button">
                        <input style="height: 26px; padding-top:2px; padding-bottom:20px;"  type="submit" class="btn btn-primary" value="Sign in" id="login">
                     </div>
                     <input type="hidden" value="login" name="command">
                     <%
                        String login_msg= (String) request.getAttribute("error");  
                        if(login_msg!=null)
                        out.println("<font color=red size=4px>"+login_msg+"</font>");
                     %>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div class="content">
         <div class="container" style="width:100%;">
            <div class="row">
               <div class="col-md-4">
                  <center>
                     <h1 style="color:#4AAF51;font-family: 'Lobster', cursive;">Welcome to IT Sharing</h1>
                  </center>
                  <img src="assets/images/share.png">
               </div>
               <div class="col-md-6">
                  <center>
                     <h1 style="color:#000;font-family: 'Lobster', cursive; font-size:50px;">Register</h1>
                  </center>
                  <br/>
                  <form class="form-horizontal" role="form" action="accountServlet" method="POST">
                     <div class="form-group">
                        <label for="firstName" class="col-sm-3 control-label" >Full Name</label>
                        <div class="col-sm-9">
                           <input type="text" id="firstName" placeholder="Your Full Name" class="form-control" required name="name">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3">Gender</label>
                        <div class="col-sm-6">
                           <div class="row">
                              <div class="col-sm-4">
                                 <label class="radio-inline">
                                 <input type="radio" id="femaleRadio" value="0" name="gender" required>Female
                                 </label>
                              </div>
                              <div class="col-sm-4">
                                 <label class="radio-inline">
                                 <input type="radio" id="maleRadio" value="1" name="gender" required>Male
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="birthDate" class="col-sm-3 control-label">Date of Birth</label>
                        <div class="col-sm-9">
                           <input type="date" id="birthDate" class="form-control" required name="dob">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="country" class="col-sm-3 control-label">Job</label>
                        <div class="col-sm-9">
                           <select id="country" class="form-control" required name="job">
                              <option selected=""></option>
                              <option>Graphic Designer</option>
                              <option>IT Network</option>
                              <option>Developer</option>
                              <option>QC Tester</option>
                              <option>Human Resource</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                           <input type="email" id="email" placeholder="Email" class="form-control" required name="email" id="email">
                           <span id="user-result"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                           <input type="password" id="password" placeholder="Password" class="form-control" required name="password">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="country" class="col-sm-3 control-label">City/Province</label>
                        <div class="col-sm-9">
                           <select id="country" class="form-control" required name="city">
                              <option selected=""></option>
                              <option>An Giang</option>
                              <option>Bà Rịa – Vũng Tàu</option>
                              <option>Bạc Liêu</option>
                              <option>Bắc Giang</option>
                              <option>Bắc Ninh</option>
                              <option>Bến Tre</option>
                              <option>Bình Thuận</option>
                              <option>Bình Định</option>
                              <option>Cao Bằng</option>
                              <option>Cần Thơ</option>
                              <option>Cà Mau</option>
                              <option>Đồng Nai</option>
                              <option>ĐắkLắk</option>
                              <option>Đồng Tháp</option>
                              <option>Điên Biên</option>
                              <option>Đồng Tháp</option>
                              <option>Đà Nẵng</option>
                              <option>Gia Lai</option>
                              <option>Hà Tĩnh</option>
                              <option>Hải Dương</option>
                              <option>Hòa Bình</option>
                              <option>Hưng Yên</option>
                              <option>Hà Nam</option>
                              <option>Hà Nội</option>
                              <option>Hồ Chí Minh</option>
                              <option>Hải Phòng</option>
                              <option>KonTum</option>
                              <option>Khánh Hòa</option>
                              <option>Kiên Giang</option>
                              <option>Lâm Đồng</option>
                              <option>Lạng Sơn</option>
                              <option>Lào Cai</option>
                              <option>Nghệ An</option>
                              <option>Nam Định</option>
                              <option>Ninh Bình</option>
                              <option>Ninh Thuận</option>
                              <option>Phú Thọ</option>
                              <option>Phú Yên</option>
                              <option>Quảng Ninh</option>
                              <option>Quảng Trị</option>
                              <option>Quảng Bình</option>
                              <option>Quảng Nam</option>
                              <option>Quảng Ngãi</option>
                              <option>Sóc Trăng</option>
                              <option>Sơn La</option>
                              <option>Tuyên Quang</option>
                              <option>Thừa Thiên Huế</option>
                              <option>Tiền Giang</option>
                              <option>Thái Bình</option>
                              <option>Thái Nguyên</option>
                              <option>Thanh Hóa</option>
                              <option>Trà Vinh</option>
                              <option>Vĩnh Phúc</option>
                              <option>Vĩnh Long</option>
                              <option>Yên Bái</option>
                           </select>
                        </div>
                     </div>
                     <center>
                        <h5>By clicking the button below, you accept our <a href="#" style="color:#4AAF51;">Terms of Use</a> and <a href="#" style="color:#4AAF51;">Privacy Policy .</a></h5>
                     </center>
                     <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                           <button type="submit" class="btn btn-primary btn-block" style="width:100px;float:right" id="register">Register</button>
                        </div>
                     </div>
                     <input type="hidden" value="register" name="command">
                  </form>
               </div>
            </div>
         </div>
      </div>
      <br/> <br/><br/>
      <div class="footer">
         <div class="container">
            <div class="row">
               <center>
                  <h5>Sharing IT@2016</h5>
               </center>
            </div>
         </div>
      </div>
      <script src="./assets/bootstrap/js/jquery-1.11.1.min.js"></script>
      <script src="./assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="./assets/bootstrap/js/jquery.backstretch.min.js"></script>
      <script src="./assets/bootstrap/js/scripts.js"></script>
   </body>
</html>