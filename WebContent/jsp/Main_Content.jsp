<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="margin-left:10px;">
    			<div class="row">
    				<div class="panel panel-default">
    					<div class="panel-body">
    						
    							<legend>Bạn đang nghĩ gì?</legend>
    						
    							<div class="form-group">
    								<label for="">Viết vào đây</label>
    								<textarea name="aPost" id="input" class="form-control" rows="3" required="required" maxlength="1000" style="resize:none"></textarea>
    							</div>
								
    							<button type="button" class="btn btn-primary pull-right" id="post">Chia sẻ</button>
    							<input type="hidden" value="post" name="command" id="command_post">
    						
    					</div>
    				</div>
    			</div>
    			
    			<div id="new-post"></div>
    			<c:forEach var="article" items="${account.articles }">
    				<div class="row">
    					<div class="panel panel-default">
    						<div class="panel-heading">
    							<div class="media">
    						<a class="pull-left" href="#">
    							<img class="media-object" src="./assets/images/avatar/${article.avatar}" alt="Image" width="50" height="auto">
    						</a>
    						<div class="media-body">
    							<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${article.idUser }">${article.nameOfUser}</a></h5>
    							<small><i>${article.dateCreate}</i></small>
    							<p>${article.content }</p>
    						</div>
    					</div>
    					<hr>
    					<ul class="list-inline">
    						<li><a id="${article.idPost }-like" onclick="like('${article.idPost}')"><span class="glyphicon glyphicon-thumbs-up"></span>Like</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-comment"></span> Bình Luận</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-link"></span> Chia Sẻ</a></li>
    					</ul>
    						</div>
    						<div class="panel-body">
    							<span class="glyphicon glyphicon-thumbs-up"></span><a href="#" id="${article.idPost }-display"> ${article.numberOfLike }</a>
    							<div id="${article.idPost }-newComment"></div>
								<c:forEach var="comment" items="${article.comments }">
    							<div class="media" >
    								<a class="pull-left" href="#">
    									<img class="media-object" width="50" height="auto" src="./assets/images/avatar/${comment.avatar}" alt="Image">
    								</a>
    								<div class="media-body">
    									<h5 class="media-heading"><a href="#">${comment.nameOfUser }</a> ${comment.content }</h5>
    									<p><a href="#">Thích</a> . <a href="#">Trả lời</a></p>
    									
    								</div>
    							</div>
    							<hr>
								</c:forEach>
									<img src="./assets/images/avatar/${account.profile.avatar}" class="img-thumbnail" width="45" alt="">
									<input type="text" id="${article.idPost }-input-comment" class="my-input" placeholder="Bình luận" maxlength="300">
									<button type="button" id="${article.idPost }-comment" onclick="newComment('${article.idPost}')" class="btn btn-primary">Bình luận</button>
									<input type="hidden" id="${article.idPost }-command-comment" name="command-comment" value="comment">
				
								
    							
    							
    						</div>
    					</div>

    					
    				
    			</div>
    			</c:forEach>
    			
    		</div>