<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
               <!-- Brand and toggle get grouped for better mobile display -->
               <div class="navbar-header" id="header_left">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="http://localhost:8080/web_it_sharing/"><span class="glyphicon glyphicon-info-sign"></span></a>
               </div>
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse navbar-ex1-collapse" id"header_right">
                  <ul class="nav navbar-nav navbar-left">
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-align-justify"></b></a>
                        <ul class="dropdown-menu">
                           <li><a href="#">Bài viết</a></li>
                           <li><a href="#">Hồ sơ</a></li>
                           <li><a href="#">Mẫu CV sáng tạo</a></li>
                           <li><a href="#">Tin đồng nghiệp</a></li>
                        </ul>
                     </li>
                  </ul>
                  <form class="navbar-form navbar-left" role="search" action="accountServlet" method="POST">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="data-search">
                     </div>
                     <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                     <input type="hidden" value="search" name="command">
                  </form>
                  <ul class="nav navbar-nav navbar-right"> 
                  	<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-user"></b></a>
                        <div class="dropdown-menu" id="my-size-dropdown">
	                        <c:forEach var="requesOfFriend" items="${account.requesOfFriend}">
	                        <div class="row">
	                        	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	                        		<div class="media">
	                        			<a class="pull-left" href="#">
	                        				<img class="media-object img-circle" src="./assets/images/avatar/${requesOfFriend.avatar}" alt="Image" width="60" height="60">
	                        			</a>
	                        			<div class="media-body">
	                        				<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${requesOfFriend.id_user }">${requesOfFriend.name}</a></h5>
	                        				<p>Muốn kết nối với bạn</p>
	                        			</div>
	                        			
	                        		</div>
	                        	</div>
	                        	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
		                        	 <form class="form-horizontal" action="accountServlet" method="POST">
		                        		<button type="submit" class="btn btn-primary" name="agreeAFriend" value="${requesOfFriend.id_user }">Chấp nhận</button>
		                        		<input type="hidden" name="command" value="agreeFriend"> 
		                        	</form>
	
		                        	 <form class="form-horizontal" action="accountServlet" method="POST">
		                        		<button type="submit" class="btn btn-default" name="deleteAFriend" value="${requesOfFriend.id_user }">Xóa yêu cầu</button>
		                        		<input type="hidden" name="command" value="deleteFriend"> 
		                        	</form>
	                        	</div>

	                        	</div>
	                        	</c:forEach>
                        </div>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-envelope"></b></a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-globe"></b></a>
                        
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="./assets/images/avatar/${account.profile.avatar}" alt="" width="20" height="20">${account.profile.name}<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                           <li><a href="accountServlet?command=changeProfile">Thiết lập tài khoản</a></li>
                           <li><a href="#">Thiết lập nội dung</a></li>
                           <li><a href="#">Thiết lập quyền riêng tư</a></li>
                           <li><a href="accountServlet?command=logout">Đăng xuât</a></li>
                        </ul>
                     </li>
                     
                  </ul>
               </div>
               <!-- /.navbar-collapse -->
            </div>
         </nav>