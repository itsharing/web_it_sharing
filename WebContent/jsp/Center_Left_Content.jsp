<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="row">
    				<div class="well">
    					<p><strong>About </strong><small><a href="#">. Edit</a></small></p>
    					<div class="row">
    						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    							<ul class="my-group-list">
    								<li><span class="glyphicon glyphicon-sunglasses"></span></li>
    								<li><span class="glyphicon glyphicon-list-alt"></span></li>
    								<li><span class="glyphicon glyphicon-home"></span></li>
    								<li><span class="glyphicon glyphicon-phone"></span></li>
    								
    							</ul>
    						</div>
    						<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
    							<ul class="my-group-list">
    								<li>Tên: <a href="#">${account.profile.name}</a></li>
    								<li>Làm việc tại: <a href="#">${account.profile.job}</a></li>
    								<li>Đang sống tại: <a href="#">${account.profile.city}</a></li>
    								<li>SĐT: <a href="#">${account.profile.phone}</a></li>
    								
    							</ul>
    						</div>
    					</div>
    				</div>
    			</div>