<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="row">
    				<div class="well">
    					<div class="row text-center">
    						<form action="accountServlet" method="POST">
    							<p><a href="accountServlet?command=changeImage"><img src="./assets/images/avatar/${account.profile.avatar}" alt="" class="img-circle" width="100" height="100"></a></p>
    								<input type="file" class="form-control" name="avatar">
          							<button type="submit" class="btn btn-default">Đổi ảnh</button>
          							<input type="hidden" value="changeImage" name="command">
    						</form>
    						
    						
	    					<p><a href="#"><h3>${account.profile.name}</h3></a></p>
	    					
    					</div>
    					<div class="row">
    						
    							<p class="text-center"><a data-toggle="modal" href='#modal-id' >Friend</a></p>
    								
    								<div class="modal fade" id="modal-id">
    									<div class="modal-dialog">
    										<div class="modal-content">
    											<div class="modal-header">
    												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    												<h4 class="modal-title">Your friends</h4>
    											</div>
    											
    											<form class="form-horizontal" action="accountServlet" method="POST">
    											<div class="modal-body">
    											<c:forEach var="friend" items="${account.friendList}">
    												<div class="media">
    													<a class="pull-left" href="#">
    														<img class="media-object img-circle" src="./assets/images/avatar/${friend.avatar}" alt="Image" width="50" height="50">
    													</a>
    													
    													<div class="media-body">
    														<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${friend.id_user }">${friend.name}</a></h5>
    														<small style="color:gray;">Friend</small>
    														<button type="submit" class="btn btn-default pull-right" name="deleteAFriend" value="${friend.id_user }">Hủy kết bạn</button>
    													</div>
    												</div>
    											</c:forEach>
    												
    												
    											</div>
    											<div class="modal-footer">
    												<button type="button" class="btn btn-default" data-dismiss="modal" name="deleteAFriend" value="${friend.id_user }">Close</button>
    												<button type="button" class="btn btn-primary">Save changes</button>
    											</div>
    											<input type="hidden" name="command" value="deleteFriend"> 
    											</form>
    										</div>
    									</div>
    								</div>
    							<p class="text-center"><strong>${account.size }</strong></p>
    						
    						
    					</div>
    				</div>
    			</div>