<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="margin-left:10px;">
    			<div class="row">
    				<div class="well">
    					<p><strong>Likes </strong><small><a href="#">. view all</a></small></p>
    					<ul class="my-group-list">
    					<c:forEach var="profile" items="${account.peopleMayKnow }">
    						<li>
    							<div class="media">
    								<a class="pull-left" href="#">
    									<img class="media-object img-circle" src="./assets/images/avatar/${profile.avatar}" alt="Image" width="40" height="40">
    								</a>
    								<div class="media-body">
    									<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${profile.id_user }">${profile.name}</a></h5>
    									<form class="form-horizontal" action="accountServlet" method="POST">
									    	<button type="submit" class="btn btn-primary pull-right" name="addAFriend" value="${profile.id_user }">Kết bạn</button>
									    	<input type="hidden" name="command" value="addFriend">
									</form>	     
    								</div>
    							</div>
    						</li>
    					</c:forEach>
    						
    						
    					</ul>
    				</div>
    			</div>
    			<div class="row">
    				<div class="well">
    					<span class="glyphicon glyphicon-copyright-mark"></span> 2016 IT SHARING
    					<a href="#">About</a>
    					<a href="#">Help</a>
    					<a href="#">Terms</a>
    					<a href="#">Privacy</a>
    					<a href="#">Cookies</a>
    					<a href="#">Ads</a>
    					<a href="#">Info</a>
    					<a href="#">Brand</a>
    					<a href="#">Blog</a>
    					<a href="#">Status</a>
    					<a href="#">Apps</a>
    					<a href="#">Jobs</a>
    					<a href="#">Advertise</a>
    				</div>
    			</div>
    		</div>