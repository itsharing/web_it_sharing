<!DOCTYPE html>
<%@page import="org.pnv.model.Account"%>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Verify Email</title>
	
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/bootstrap/css/font-awesome.min.css">

</head>
<body style="background-image: url('./assets/images/backgrounds/1.jpg');color:#fff;font-size:15px;">
	<div class="container" style="width:500px">
        <form class="form-horizontal" role="form" action="accountServlet" method="POST">
        	<center><h1 style="color:#4AAF51;font-family:Arial;">Complete registration</h1></center><br/><br/>
          	<label><h4 style="font-family:Arial;font-size:18px">&#160&#160&#160&#160&#160Please check your email to know your verify code</h4></label><br/><br/><br/>
                <div class="form-group">
                
                    <div class="col-sm-9" style="margin-left:70px">
                        <% 	String email = (String)request.getAttribute("email");
                    		out.write("<input type ='hidden' value='" + email + "' name='email' />");
                    	%>
                    	<% 	String password = (String)request.getAttribute("password");
                    		out.write("<input type ='hidden' value='" + password + "' name='password' />");
                    	%>
                    </div>
                </div>
                <div class="form-group">
                
                    <div class="col-sm-9" style="margin-left:70px">
                    <%
				String login_msg= (String) request.getAttribute("error");  
					if(login_msg!=null)
					out.println("<font color=red size=4px>"+login_msg+"</font>");
								%>
                        <input type="text" id="firstName" placeholder="Please enter your verify code" class="form-control" autofocus required name="verifyCode" pattern="\d{6}">
                    </div>
                </div>
                
                
                
                
                <br/><br/>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3" style="margin-left:180px">
                        <button type="submit" class="btn btn-primary btn-block" style="width:100px;">Submit</button>
                    </div>
                </div>
                <input type="hidden" value="verifyCode" name="command">
        </form>
    </div>
</body>
</html>