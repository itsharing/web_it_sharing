<%@page import="org.pnv.model.Account"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IT SHARING</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="./assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./assets/bootstrap/css/style2.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="./assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      
      
	<script>
	
		$(document).ready(function(){
		    $("#post").click(function(){
		    	var input = $("#input").val();
		    	var command = $("#command_post").val();
		        $.ajax({
		        	type: "POST",
		        	url: "postServlet", 
		        	data:{input: input, command: command},
		        	success: function(result){
				    	$("#new-post").after(result.html);
				    	$("#input").val(null);
		        	}
		        	
		        });
		    });
		    
		   
		});

		
		
	</script>
	<script type="text/javascript">
		function newComment(idPost){
			$(document).ready(function(){
				if(!$.trim($("#"+idPost+"-input-comment").val()))
	        	{
	        	    alert("Please enter some text in the comment");
	        	} else{
	        		var input = $("#"+idPost+"-input-comment").val();
			    	var command = $("#"+idPost+"-command-comment").val();
			        $.ajax({
			        	type: "POST",
			        	url: "postServlet", 
			        	data:{input: input, command: command, idPost:idPost},
			        	success: function(result){
					    	$("#"+idPost+"-newComment").before(result.html);
					    	$("#"+idPost+"-input-comment").val(null);
					    	
			        	}
			        	
			        });
	        	}
				    	
				   
				});
		}
	</script>
	<script type="text/javascript">
    	function like(idPost){
    		$(document).ready(function(){
    			var obj = $("#"+idPost+"-like");
    	        if( obj.data('liked')){
    	        	obj.data('liked', false);
    	            $.ajax({
    	                 type: "GET",                     
    	                 url: "postServlet", 
    	                 dataType: "text",
    	                 data:{idPost: idPost, command: "unlikePost" },
    	                 success: function(result){
    	                    $("#"+idPost+"-display").html(result);  
    	                    obj.html('<span class="glyphicon glyphicon-thumbs-up"></span>Unlike');
    	                 }                     
    	             });   
    	            
    	        }
    	        else{   
    	        	obj.data('liked', true);
    	              $.ajax({
    	                 type: "GET",                     
    	                 url: "postServlet", 
    	                 dataType: "text",
    	                 data:{idPost: idPost, command: "likePost" },
    	                 success: function(result){
    	                	 $("#"+idPost+"-display").html(result);    
    	                	 obj.html('<span class="glyphicon glyphicon-thumbs-up"></span>Like');
    	                      }                     
    	                });  
    	            
    	        }
    	    });
    	}

	</script>
	
	
</head>
<body>
	<%@include file="Header.jsp" %>
    <div class="container" style="margin-top:70px">
    	<div class="row">
    		<%@include file="Left_Content.jsp" %>
    		<%@include file="Main_Content.jsp" %>
    		<%@include file="Right_Content.jsp" %>
    	</div>
    </div>
</body>
</html>