<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

    				<div class="row">
    					<div class="panel panel-default">
    						<div class="panel-heading">
    							<div class="media">
    							
    						<a class="pull-left" href="#">
    							<img class="media-object" src="./assets/images/avatar/${newArticle.avatar}" alt="Image" width="50" height="auto">
    						</a>
    						
    						<div class="media-body">
    							<h5 class="media-heading"><a href="#">${newArticle.nameOfUser}</a></h5>
    							<small><i>${newArticle.dateCreate}</i></small>
    							<p>${newArticle.content}</p>
    						</div>
    					</div>
    					<hr>
    					<ul class="list-inline">
    						<li><a id="${newArticle.idPost }-like" onclick="like('${newArticle.idPost}')"><span class="glyphicon glyphicon-thumbs-up"></span>Like</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-comment"></span> Bình Luận</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-link"></span> Chia Sẻ</a></li>
    					</ul>
    						</div>
    						<div class="panel-body">
    							<span class="glyphicon glyphicon-thumbs-up"></span><a id="${newArticle.idPost }-display"> ${newArticle.numberOfLike }</a>
    							<div id="${newArticle.idPost }-newComment"></div>
								<c:forEach var="comment" items="${newArticle.comments }">
    							<div class="media" >
    								<a class="pull-left" href="#">
    									<img class="media-object" width="50" height="auto" src="./assets/images/avatar/${comment.avatar}" alt="Image">
    								</a>
    								<div class="media-body">
    									<h5 class="media-heading"><a href="#">${comment.nameOfUser }</a> ${comment.content }</h5>
    									<p><a href="#">Thích</a> . <a href="#">Trả lời</a></p>
    									
    								</div>
    							</div>
    							<hr>
								</c:forEach>
									<img src="./assets/images/avatar/${newArticle.avatar}" class="img-thumbnail" width="45" alt="">
									<input type="text" id="${newArticle.idPost }-input-comment" class="my-input" placeholder="Bình luận" maxlength="300">
									<button type="button" id="${newArticle.idPost }-comment" onclick="newComment('${newArticle.idPost}')" class="btn btn-primary">Bình luận</button>
									<input type="hidden" id="${newArticle.idPost }-command-comment" name="command-comment" value="comment">
				
								
    							
    							
    						</div>
    					</div>

    					
    				
    			</div>
   