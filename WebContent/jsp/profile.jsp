<%@page import="org.pnv.model.Account"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IT SHARING</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="./assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./assets/bootstrap/css/style2.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function checkPasswordMatch() {
        var password = $("#newPassword").val();
        var confirmPassword = $("#confirmPassword").val();

        if (password != confirmPassword) {
            $("#message").html("<div style='color:red'>Passwords do not match!</div>");
        	document.getElementById("saving").disabled = true;
    	} else {
            $("#message").html("<div style='color:green'>Passwords match.</div>");
            document.getElementById("saving").disabled = false;
    	}
    }
    </script>
</head>
<body>
	<%@include file="Header.jsp" %>
    <div class="container" style="margin-top:70px">
    	<div class="row">
    		<%@include file="Left_Content.jsp" %>
    		
    		<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="margin-left:10px;">
                <div class="row">
                    <div class="well">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Thông tin tài khoản</a>
                                </li>
                                <li role="presentation">
                                    <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Thông tin cá nhân</a>
                                </li>
                            </ul>
                        
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                          
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <div class="form-group">
									            <label class="col-lg-3 control-label"><strong>Email: </strong></label>
									            <div class="col-lg-8" >
									        		${account.email}
									            </div> 
									         </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="form-group">
									            <label class="col-lg-3 control-label"><strong>Password: </strong></label>
									            <div class="col-lg-8" >**********
									            </div> 
									         </div>
                                        </li>
                                        <li class="list-group-item">
                                            <a class="btn btn-primary" data-toggle="modal" href='#modal-id-2'>Change password</a>
                                             <br></br>
					                         
				                                         	 <%
															String pass_msg= (String) request.getAttribute("note");  
																if(pass_msg!=null)
																out.println("<font color=red size=3px>"+pass_msg+"</font>");
															%>             
															
										      
                                            <div class="modal fade" id="modal-id-2">
                                                <div class="modal-dialog">
                                                	<form class="form-horizontal" action="accountServlet" method="POST">
                                                	
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Change password</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <B>Old password:</B> <input class="form-control" type="password" name="oldPassword" autofocus required placeholder="**********" id="oldPassword">

                                                            <B>New password:</B> <input class="form-control" type="password" name="newPassword" required placeholder="**********" id="newPassword">

                                                            <B>Confirm password:</B> <input class="form-control" type="password" name="confirmPassword" required placeholder="**********" id="confirmPassword" onChange="checkPasswordMatch();">
															<span id="message"></span>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary" id="saving">Save</button>
                                                            <input type="hidden" name="command" value="changePassword"> 
				                                         	
                                                           
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab">
                                    <ul class="list-group">
                                        <li class="list-group-item">
	                                        <div class="form-group">
									            <label class="col-lg-3 control-label"><strong>Fullname: </strong></label>
									            <div class="col-lg-8" >
									        		${account.profile.name}
									            </div> 
									         </div>
                                        </li>
                                        <li class="list-group-item">
                                        	<div class="form-group">
									            <label class="col-lg-3 control-label"><strong>Gender: </strong></label>
									            <div class="col-lg-8" >	
		                                           <c:choose>
															<c:when test="${account.profile.gender eq 0}">
																<c:out value="Female"></c:out>
															</c:when>
															<c:when test="${account.profile.gender eq 1}">
																<c:out value="Male"></c:out>
															</c:when>
													</c:choose>
												</div>
											</div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="form-group">
									            <label class="col-lg-3 control-label"><strong>Date of birth: </strong></label>
									            <div class="col-lg-8" >
									        		${account.profile.dob}
									            </div> 
									         </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="form-group">
									            <label class="col-lg-3 control-label"><strong>Phone: </strong></label>
									            <div class="col-lg-8" >
									        		${account.profile.phone}
									            </div> 
									         </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="form-group">
									            <label class="col-lg-3 control-label"><strong>City/Province: </strong></label>
									            <div class="col-lg-8" >
									        		${account.profile.city}
									            </div> 
									         </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="form-group">
									            <label class="col-lg-3 control-label"><strong>Job: </strong></label>
									            <div class="col-lg-8" >
									        		${account.profile.job}
									            </div> 
									         </div>
                                        </li>
                                        <li class="list-group-item">
                                            <a class="btn btn-primary" data-toggle="modal" href='#modal-id-1'>Change profile</a>
                                             <br></br>
								                <%
															String profile_msg= (String) request.getAttribute("note");  
																if(profile_msg!=null)
																out.println("<font color=red size=3px>"+profile_msg+"</font>");
														%>   
															
                                            <div class="modal fade" id="modal-id-1">
                                                <div class="modal-dialog">
                                                	<form class="form-horizontal" action="accountServlet" method="POST">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Your profile</h4>
                                                        </div>
                                                        <div class="modal-body" style="height: 300px">
                                                            <form class="form-inline">
                                                                
                                                                <label class="col-lg-3 control-label">Full name:</label>
                                                                <div class="col-lg-8">
                                                                    <input class="form-control" style="width: 100%;" type="text" autofocus required name="name"
                                                                       value= "${account.profile.name}">
                                                                </div>
                                                                
                                                                <br></br>
                                                                
                                                                <label class="col-lg-3 control-label">Gender:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="row">
                                                                    	<c:choose>
																			<c:when test="${account.profile.gender eq 0}">
																				<c:set var="checkFemale" value="checked"></c:set>
																				<c:set var="checkMale" value="unchecked"></c:set>
																			</c:when>
																			<c:when test="${account.profile.gender eq 1}">
																				<c:set var="checkFemale" value="unchecked"></c:set>
																				<c:set var="checkMale" value="checked"></c:set>
																			</c:when>
																		</c:choose>
                                                                        <div class="col-sm-4">
                                                                            <label class="radio-inline"> 
                                                                            <input type="radio"
                                                                                id="femaleRadio" value="0" name="gender" required <c:out value="${checkFemale}"/>>Female
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <label class="radio-inline"> <input type="radio"
                                                                                id="maleRadio" value="1" name="gender" required <c:out value="${checkMale}"/>>Male
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br></br>


                                                               <label for="birthDate" class="col-lg-3 control-label">Date
                                                                    of Birth:</label>
                                                                <div class="col-lg-8">
                                                                    <input name="dob" type="date" style="width: 100%;" id="birthDate" value="${account.profile.dob}" class="form-control" required>
                                                                </div>
                                                                <br></br>
                                                           

                                                            
                                                                <label class="col-lg-3 control-label">Phone:</label>
                                                                <div class="col-lg-8">
                                                                      <input class="form-control" type="tel" style="width: 100%;"  name = "phone" value="${account.profile.phone}" placeholder="Ex: 09xxxxxxxx, 01xxxxxxxxx" pattern="^\d{10}|\d{11}$">
                                                                </div>
                                                                <br></br>
                                                          

                                                            
                                                                <label for="country" class="col-lg-3 control-label">City/Province</label>
                                                                <div class="col-lg-8">
                                                                    <select autofocus required name="city" style="width: 100%;" id="country" class="form-control">
                                                                          <option><c:out value = "${account.profile.city}" /></option>
                                                                          <option>An Giang</option>
											                              <option>Bà Rịa – Vũng Tàu</option>
											                              <option>Bạc Liêu</option>
											                              <option>Bắc Giang</option>
											                              <option>Bắc Ninh</option>
											                              <option>Bến Tre</option>
											                              <option>Bình Thuận</option>
											                              <option>Bình Định</option>
											                              <option>Cao Bằng</option>
											                              <option>Cần Thơ</option>
											                              <option>Cà Mau</option>
											                              <option>Đồng Nai</option>
											                              <option>ĐắkLắk</option>
											                              <option>Đồng Tháp</option>
											                              <option>Điên Biên</option>
											                              <option>Đồng Tháp</option>
											                              <option>Đà Nẵng</option>
											                              <option>Gia Lai</option>
											                              <option>Hà Tĩnh</option>
											                              <option>Hải Dương</option>
											                              <option>Hòa Bình</option>
											                              <option>Hưng Yên</option>
											                              <option>Hà Nam</option>
											                              <option>Hà Nội</option>
											                              <option>Hồ Chí Minh</option>
											                              <option>Hải Phòng</option>
											                              <option>KonTum</option>
											                              <option>Khánh Hòa</option>
											                              <option>Kiên Giang</option>
											                              <option>Lâm Đồng</option>
											                              <option>Lạng Sơn</option>
											                              <option>Lào Cai</option>
											                              <option>Nghệ An</option>
											                              <option>Nam Định</option>
											                              <option>Ninh Bình</option>
											                              <option>Ninh Thuận</option>
											                              <option>Phú Thọ</option>
											                              <option>Phú Yên</option>
											                              <option>Quảng Ninh</option>
											                              <option>Quảng Trị</option>
											                              <option>Quảng Bình</option>
											                              <option>Quảng Nam</option>
											                              <option>Quảng Ngãi</option>
											                              <option>Sóc Trăng</option>
											                              <option>Sơn La</option>
											                              <option>Tuyên Quang</option>
											                              <option>Thừa Thiên Huế</option>
											                              <option>Tiền Giang</option>
											                              <option>Thái Bình</option>
											                              <option>Thái Nguyên</option>
											                              <option>Thanh Hóa</option>
											                              <option>Trà Vinh</option>
											                              <option>Vĩnh Phúc</option>
											                              <option>Vĩnh Long</option>
											                              <option>Yên Bái</option>
                                                                    </select>
                                                                </div>
                                                                <br></br>
                                                        
                                                                <label for="country" class="col-lg-3 control-label">Job:</label>
                                                                <div class="col-lg-8">
                                                                    <select autofocus required  style="width: 100%;" name="job" id="job" class="form-control" >
                                                                        <option><c:out value = "${account.profile.job}" /></option>
                                                                        <option >Graphic Designer</option>
                                                                        <option>IT Network</option>
                                                                        <option>Developer</option>
                                                                        <option>QC Tester</option>
                                                                        <option>Human Resource</option>
                                                                    </select>
                                                                </div>

                                                           
                                                            </form>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                                            <input type="hidden" name="command" value="changeProfile"> 
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       									
														
														
                   </div>
                </div>
            </div>
    		
    		<%@include file="Right_Content.jsp" %>
    	</div>
    </div>
</body>
</html>