<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IT SHARING</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="./assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./assets/bootstrap/css/style2.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="./assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<%@include file="Header.jsp" %>
    <div class="container" style="margin-top:70px">
    	<div class="row">
    		<%@include file="Left_Content.jsp" %>
    		<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="margin-left:10px;">
    			
    			
    				<div class="row">    			
    				<%
                        String searchFail= (String) request.getAttribute("searchFail");  
                        if(searchFail!=null)
                        out.println("<font color=black size=4px>"+searchFail+"</font>");
                     %>
                     <%
                     	String searchSuccess = (String) request.getAttribute("searchSuccess");
	                     if(searchSuccess!=null)
                         out.println("<font color=black size=4px>"+searchSuccess+"</font>");
                     %>
    				<div class="panel panel-default">
    				
    				<!-- this is one form of user -->
   
    				<c:forEach var="searchResult" items="${list}">
    					<div class="panel-body">
    						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    						<div class="media">
		    					<a class="pull-left" href="#">
		    						<img class="media-object img-circle" width="50" height="50" src="./assets/images/avatar/${searchResult.profile.avatar }" alt="Image">
		    					</a>
		    					<div class="media-body">
		    						<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${searchResult.id}">${searchResult.profile.name}</a></h5>
		    						<p><small style="color:gray;">Thành viên . 18 giờ trước</small></p>
		    					</div>
    						</div>
    					</div>
    					<c:set var="check"  value="${1}"/>
    						<c:forEach var="addfriend" items="${account.relationShip}">
				    			<c:choose>
								    <c:when test="${addfriend.id_friend == searchResult.id && addfriend.status == 1}"><!-- Đã là bạn -->
									       <form class="form-horizontal" action="accountServlet" method="POST">
										    		<button type="submit" class="btn btn-primary pull-right" name="deleteAFriend" value="${searchResult.id }">Hủy bạn</button>
										    		<input type="hidden" name="command" value="deleteFriend"> 
										    		<c:set var="check"  value="${0 }"/>
											</form>   
								    </c:when>
								     <c:when test="${addfriend.id_friend == searchResult.id && addfriend.status == 0}"><!-- Đang chờ trả lời mời --> 
									        <form class="form-horizontal" action="accountServlet" method="POST">
										    		<button type="submit" class="btn btn-primary pull-right" name="deleteAFriend" value="${searchResult.id }">Hủy mời</button>
										    		<input type="hidden" name="command" value="deleteFriend"> 
										    		<c:set var="check"  value="${0}"/>
											</form>    
								    </c:when>
								     <c:when test="${addfriend.id_friend == searchResult.id && addfriend.status == 2}"><!-- Chấp nhận kết bạn -->
									       <form class="form-horizontal" action="accountServlet" method="POST">
										    		<button type="submit" class="btn btn-primary pull-right" name="agreeAFriend" value="${searchResult.id }">Chấp nhận</button>
										    		<input type="hidden" name="command" value="agreeFriend"> 
										    		<c:set var="check"  value="${0 }"/>
											</form>   
								    </c:when>
								</c:choose>
	                    	</c:forEach>
	                    	<c:choose>
	                    		<c:when test="${check==1}">
									<form class="form-horizontal" action="accountServlet" method="POST">
									    	<button type="submit" class="btn btn-primary pull-right" name="addAFriend" value="${searchResult.id }">Kết bạn</button>
									    	<input type="hidden" name="command" value="addFriend">
									</form>	          
								</c:when>
							</c:choose>
    					</div>
    				</c:forEach>
    					
    				</div>
    				
    			</div>
    	
    		</div>
    		<%@include file="Right_Content.jsp" %>
    	</div>
    </div>
</body>
</html>