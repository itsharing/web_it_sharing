package org.pnv.dao;


import java.util.List;
import org.pnv.model.Account;
import org.pnv.model.Article;
import org.pnv.model.Comment;
import org.pnv.model.Profile;
import org.pnv.model.SearchResult;


public interface AccountDao {
	
	int selectDataToCheckLogin(String email, String password);
	boolean insertNewAccount(Account account);
	boolean updateStatus(String email);
	String selectCode(String email);
	boolean selectEmail(String email);	
	boolean isUpdatePasswordSuccessful(String email, String oldPassword, String newPassword);
	Account selectAccount(String email, String password);
	Account selectProfile(Account account);
	boolean isUpdateInformationSuccessfull(Account account);
	boolean isUpdateAvatarSuccessful(String linkAvatar, int id);
	String selectAvatar(int id);
	Account selsectArticalOfUser(Account account);
	Account insertArticalOfUser(Account account,String content);
	boolean updateArticalOfUser(int id_user,int id_post,String content);
	boolean deletetArticalOfUser(int id_post,int id_user);
	Profile selectOneProfile(int id_user);
	Account selecetArticleWithArticle(Account account);
	Account selectAccount(int idUser);
	List<SearchResult> search(String nameUserSearch);
	Account selectFriendOfUser(Account account);
	boolean isUpdateInformationSuccessfull(Account account, String name, int gender, String dob, String job,
			String city, String phone);
	boolean deleteFriend(Account account, String id_friend);
	boolean agreeFriend(Account account, String id_friend);
	boolean addFriend(Account account, String id_friend);
	Account selectFriendsRequest(Account account);
	boolean insertArticalOfUser(int id_user, String content);
	Account selectStatusFriend(Account account);
	Account selectPeopleMayKnow(Account account);
	Account insertComment(Account account, String content, int idPost);
	Account selectComment(Account account);
	List<Comment> selectCommentWithidPost(int idPost, int idUser);
    public boolean insertLike(int idUser, int id_post);
    public int selectNumberOfLike(int id_user, int id_post);
    public boolean deleteLike(int idUser, int id_post);
    

}
