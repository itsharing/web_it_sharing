package org.pnv.dao;


import java.util.List;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.pnv.model.Account;
import org.pnv.model.Article;
import org.pnv.model.Comment;
import org.pnv.model.Profile;
import org.pnv.model.RelationShip;
import org.pnv.model.SearchResult;
import org.pnv.util.DBHelper;
import org.pnv.util.Utils;

public class AccountDaoImpl implements AccountDao {
	
	@Override
	public int selectDataToCheckLogin(String email, String password) {
		String sql = "SELECT isActive FROM account WHERE email = ? AND password = ?";

		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = -1;
		try {
			ps = cn.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, Utils.convertPasswordToMD5(password));
			rs = ps.executeQuery();
			if(rs.next()){
				result = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return result;
		
	}

	@Override
	public boolean insertNewAccount(Account account) {
		Connection cn = DBHelper.returnConnection();
		CallableStatement ps = null;
		int result = 0;
		try {
			String sql = "CALL insert_account(?, ?, ?, ?, ?, ?, ?, ?,?)";
			ps = cn.prepareCall(sql);
			ps.setString(1, account.getEmail());
			ps.setString(2, Utils.convertPasswordToMD5(account.getPassword()));
			ps.setString(3, account.getVerifyCode());
			ps.setString(4, account.getProfile().getName());
			ps.setString(5, account.getProfile().getDob());
			ps.setInt(6, account.getProfile().getGender());
			ps.setString(7, account.getProfile().getCity());
			ps.setString(8, account.getProfile().getJob());
			ps.registerOutParameter(9, Types.INTEGER);
			
			ps.executeUpdate();
			result = ps.getInt(9);
		} catch (SQLException ex) {
			return false;
		}finally {
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		if(result == 0){
			return false;
		}else{
			return true;
		}
		
	}

	public boolean updateStatus(String email) {
		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		try {
			
			String sql = "UPDATE account SET isActive = 1 WHERE email = ?";
			ps = cn.prepareStatement(sql);
			ps.setString(1, email);

			int result = ps.executeUpdate();
			if (result == -1) {
				return false;
			}else{
				return true;
			}
		} catch (SQLException ex) {
			
		}finally {
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return false;

	}

	public String selectCode(String email) {
		String sql = "SELECT verify_code FROM account WHERE email = ?";

		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		try {
			ps = cn.prepareStatement(sql);

			ps.setString(1, email);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return result;

	}

	@Override
	public boolean selectEmail(String email) {
		String sql = "SELECT email FROM account WHERE email = ?";

		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = cn.prepareStatement(sql);

			ps.setString(1, email);
			rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			System.out.println("something went wrong!");
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return false;
	}
	
	

	@Override
	public boolean isUpdatePasswordSuccessful(String email, String oldPassword, String newPassword) {
		String sql = "UPDATE account SET password = ? WHERE email = ? AND password = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, Utils.convertPasswordToMD5(newPassword));
			prepare.setString(2, email);
			prepare.setString(3, Utils.convertPasswordToMD5(oldPassword));
			
			int check = prepare.executeUpdate();
			
			if(check == 1){
				cn.close();
				return true;
			}else{
				cn.close();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Account selectAccount(String email, String password) {
		Account account = null; 
		String sql = "SELECT id_user,isActive,verify_code,date_create,date_update FROM account WHERE email = ? AND password = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, Utils.convertPasswordToMD5(password));	
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				account = new Account();
				account.setIdUser(rs.getInt(1));
				account.setEmail(email);
				account.setPassword(Utils.convertPasswordToMD5(password));
				account.setIsActive(rs.getInt(2));
				account.setVerifyCode(rs.getString(3));
				account.setDateCreate(rs.getString(4));
				account.setDateUpdate(rs.getString(5));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}
	
	@Override
	public Account selectAccount(int idUser) {
		Account account = null; 
		String sql = "SELECT email,password,isActive,verify_code,date_create,date_update FROM account WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, idUser);
			
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				account = new Account();
				account.setIdUser(idUser);
				account.setEmail(rs.getString(1));
				account.setPassword(rs.getString(2));
				account.setIsActive(rs.getInt(3));
				account.setVerifyCode(rs.getString(4));
				account.setDateCreate(rs.getString(5));
				account.setDateUpdate(rs.getString(6));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}

	@Override
	public Account selectProfile(Account account) {
		Profile profile = null;
		String sql = "SELECT name,dob,gender,phone,job,city,avatar FROM profile WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, account.getIdUser());
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				profile = new Profile();
				profile.setName(rs.getString(1));
				profile.setDob(rs.getString(2));
				profile.setGender(rs.getInt(3));
				profile.setPhone(rs.getString(4));
				profile.setJob(rs.getString(5));
				profile.setCity(rs.getString(6));
				profile.setAvatar(rs.getString(7));
				account.setProfile(profile);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}

	@Override
	public boolean isUpdateInformationSuccessfull(Account account) {
		
		String sql = "UPDATE profile SET name = ?, dob = ?, gender = ?, phone = ?, job = ?, city = ? WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, account.getProfile().getName() );
			prepare.setString(2, account.getProfile().getDob());
			prepare.setInt(3, account.getProfile().getGender());
			prepare.setString(4, account.getProfile().getPhone() );
			prepare.setString(5, account.getProfile().getJob());
			prepare.setString(6, account.getProfile().getCity());
			prepare.setInt(7,account.getIdUser());
			
			int result = prepare.executeUpdate();
			
			if(result == -1){
				cn.close();
				return false;
			}else{
				cn.close();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	@Override
	public boolean isUpdateInformationSuccessfull(Account account, String name, int gender, String dob, String job, String city, String phone) {
		
		String sql = "UPDATE profile SET name = ?, dob = ?, gender = ?, phone = ?, job = ?, city = ? WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, name );
			prepare.setString(2, dob);
			prepare.setInt(3,gender);
			prepare.setString(4, phone);
			prepare.setString(5, job);
			prepare.setString(6, city);
			prepare.setInt(7,account.getIdUser());
			
			int result = prepare.executeUpdate();
			
			if(result == -1){
				cn.close();
				return false;
			}else{
				cn.close();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public boolean isUpdateAvatarSuccessful(String linkAvatar, int id) {
		String sql = "UPDATE profile SET avatar = ? WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, linkAvatar);
			prepare.setInt(2, id);
			
			int result = prepare.executeUpdate();
			if( result == 1 ){
				cn.close();
				return true;
				
			}else{
				cn.close();
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public String selectAvatar(int id) {
		String sql = "SELECT avatar FROM profile WHERE id_user = ?";

		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		try {
			ps = cn.prepareStatement(sql);

			ps.setInt(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return result;
	}

	@Override
	public Account selsectArticalOfUser(Account account) {
		String sql = "SELECT * FROM post WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		List<Article> articleList = new ArrayList<Article>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				Article article = new Article();
				article.setIdPost(result.getInt(1));
				article.setIdUser(result.getInt(2));
				article.setContent(result.getString(3));
				article.setDateCreate(result.getString(4));
				article.setDateUpdate(result.getString(5));
				articleList.add(article);
			}
			cn.close();
			account.setArticles(articleList);
		}catch(Exception e){
			System.out.println("Error function selsectArticalOfUser ");
		}
		return account;	
	}

	@Override
	public boolean insertArticalOfUser(int id_user, String content) {
		String sql = "INSERT INTO  id_user = ?,content = ?,date_create = now() FROM post";
		Connection cn = DBHelper.returnConnection();
		try{
			PreparedStatement prepare = cn.prepareStatement(sql);
			String id = Integer.toString(id_user);
			prepare.setString(1, id );
			prepare.setString(2, content);
			int result = prepare.executeUpdate();
			if( result == 1){
				cn.close();
				return true;
			}else {
				cn.close();
				return false;
			}
		}catch(Exception e){
			System.out.println("Error function insertArticalOfUser ");
		}
		return false;
	}

	@Override
	public boolean updateArticalOfUser(int id_user, int id_post, String content) {
		String sql = "UPDATE post SET  content = ? ,date_update = now() WHERE id_post = ? AND id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try{
			PreparedStatement prepare = cn.prepareStatement(sql);
			String idUser  = Integer.toString(id_user);
			String idPost = Integer.toString(id_post);
			prepare.setString(1, content);
			prepare.setString(2, idPost);
			prepare.setString(3,  idUser );
			int result = prepare.executeUpdate();
			if( result == 1){
				cn.close();
				return true;
			}else {
				cn.close();
				return false;
			}
		}catch(Exception e){
			System.out.println("Error function updateArticalOfUser ");
		}
		return false;
	}

	@Override
	public boolean deletetArticalOfUser(int id_post, int id_user) {
		String sql = "DELETE FROM post WHERE id_post = ? AND is_user = ?";
		Connection cn = DBHelper.returnConnection();
		try{
			PreparedStatement prepare = cn.prepareStatement(sql);
			String idUser  = Integer.toString(id_user);
			String idPost = Integer.toString(id_post);
			prepare.setString(1, idPost);
			prepare.setString(2,  idUser );
			int result = prepare.executeUpdate();
			if( result == 1){
				cn.close();
				return true;
			}else {
				cn.close();
				return false;
			}
		}catch(Exception e){
			System.out.println("Error function deletetArticalOfUser ");
		}
		return false;
		
	}
	@Override
	public Account selecetArticleWithArticle(Account account) {
        AccountDao accountDaoImpt = new AccountDaoImpl();
        List<Article> articles = new ArrayList<Article>();
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String selections = String.valueOf(account.getIdUser());
        for (int i = 0; i < account.getFriendList().size(); i++) {
        	selections = selections + "," + account.getFriendList().get(i).getId_user();
        }
        
        String sql = "SELECT P.id_post, P.id_user, P.content, P.date_create, P.date_update, PF.name, PF.avatar FROM post AS P, profile AS PF WHERE P.id_user IN (" + selections + ") AND P.id_user = PF.id_user ORDER BY date_create DESC";
        try {
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();

            while(rs.next()){
                Article article = new Article();
                article.setIdPost(rs.getInt(1));
                article.setIdUser(rs.getInt(2));
                article.setContent(rs.getString(3));
                article.setDateCreate(rs.getString(4));
                article.setDateUpdate(rs.getString(5));
                article.setNameOfUser(rs.getString(6));
                article.setAvatar(rs.getString(7));
                article.setComments(accountDaoImpt.selectCommentWithidPost(rs.getInt(1),rs.getInt(2)));
                article.setNumberOfLike(accountDaoImpt.selectNumberOfLike(rs.getInt(2), rs.getInt(1)));
                articles.add(article);
            }
            account.setArticles(articles);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
        
        return account;

	}

	@Override
	public List<SearchResult> search(String nameUserSearch) {
		String sql = "SELECT id_user FROM profile WHERE name LIKE '%"+nameUserSearch +"%' ";		
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement prepare = null;
		try {
			List<SearchResult> result =  new ArrayList<>();
			prepare = cn.prepareStatement(sql);
			rs = prepare.executeQuery();				
			while(rs.next()){
				SearchResult res = new SearchResult();
				int id_user= Integer.parseInt(rs.getString(1));
				Profile profile = selectOneProfile(id_user);
				res.setId(id_user);
				res.setProfile(profile);
				result.add(res);
			}
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (prepare != null) try { prepare.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return null;
	}

	@Override
	public Profile selectOneProfile(int id_user) {
		Profile profile = null;
		String sql = "SELECT name,dob,gender,phone,job,city,avatar FROM profile WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, id_user);			
			rs = ps.executeQuery();
			
			if(rs.next()){
				profile = new Profile();
				profile.setName(rs.getString(1));
				profile.setDob(rs.getString(2));
				profile.setGender(rs.getInt(3));				
				profile.setPhone(rs.getString(4));				
				profile.setJob(rs.getString(5));
				profile.setCity(rs.getString(6));				
				profile.setAvatar(rs.getString(7));	
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return profile;
	}


	@Override
	public Account selectFriendOfUser(Account account) {
		String sql = "SELECT A.id_friend, C.name, C.avatar FROM relationship as A INNER JOIN  account as B INNER JOIN profile as C ON "
				+ "A.id_user = ? and A.id_friend = B.id_user and B.id_user = C.id_user and A.status= 1  and A.id_friend !=?";
		Connection cn = DBHelper.returnConnection();
		List<Profile> profileList = new ArrayList<Profile>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id = Integer.toString(account.getIdUser());
			prepare.setString(1, id );
			prepare.setString(2, id );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				 Profile prolile = new Profile();
				 prolile.setId_user(result.getInt(1));
				 prolile.setName(result.getString(2));
				 prolile.setAvatar(result.getString(3));
				 profileList.add(prolile);
			}
			cn.close();
			account.setFriendList(profileList);
		}catch(Exception e){
			System.out.println("Error function sselectFriendOfUser ");
		}
		return account;	
	}
	
	@Override
	public Account selectFriendsRequest(Account account) {
		String sql = "SELECT A.id_friend, C.name,C.avatar FROM relationship as A INNER JOIN  account as B INNER JOIN profile as C ON "
				+ "A.id_user = ? and A.id_friend = B.id_user and B.id_user = C.id_user and A.status= ?";
		Connection cn = DBHelper.returnConnection();
		List<Profile> profileList = new ArrayList<Profile>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id = Integer.toString(account.getIdUser());
			prepare.setString(1, id );
			prepare.setString(2, "2" );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				 Profile prolile = new Profile();
				 prolile.setId_user(result.getInt(1));
				 prolile.setName(result.getString(2));
				 prolile.setAvatar(result.getString(3));
				 profileList.add(prolile);
			}
			cn.close();
			account.setRequesOfFriend(profileList);
		}catch(Exception e){
			System.out.println( e+"Error function selectFriendsRequest ");
		}
		return account;	
	}
	@Override
	public Account selectStatusFriend(Account account) {
		String sql = "select id_friend, status from relationship where id_user = ? and id_friend !=?";
		Connection cn = DBHelper.returnConnection();
		List<RelationShip> relationShips = new ArrayList<RelationShip>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_user );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				 RelationShip relationShip = new RelationShip();
				 relationShip.setId_friend(result.getInt(1));
				 relationShip.setStatus(result.getInt(2));
				 relationShips.add(relationShip);
			}
			cn.close();
			account.setRelationShip(relationShips);
		}catch(Exception e){
			System.out.println("Error function selectFriend");
		}
		return account;	
	}
	public static void main(String[] args) {
        Account account = new Account();
        account.setIdUser(1);
       AccountDaoImpl accountDaoImpl = new AccountDaoImpl();
       accountDaoImpl.selectStatusFriend(account);
               for(int o= 0 ; o <account.getRelationShip().size();o++ ){
                     System.out.print(account.getRelationShip().get(o).getId_friend());
                     System.out.print("+");
                     System.out.print(account.getRelationShip().get(o).getStatus());
                     System.out.print("  \n");
              
               }
              
 }

	@Override
	public boolean addFriend(Account account, String id_friend) {
		String sql ="INSERT INTO relationship(id_user, id_friend,status,date_create) VALUE(?,?,?,now())";
		Connection cn = DBHelper.returnConnection();
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_friend );
			prepare.setString(3, "0" );
			prepare.executeUpdate();
			prepare= cn.prepareCall(sql);
			prepare.setString(1, id_friend );
			prepare.setString(2, id_user );
			prepare.setString(3, "2" );
			prepare.executeUpdate();
			return true;
		}catch(Exception e){
			
			System.out.print("error add friend");
			return false;
		}
	}
	
	@Override
	public boolean agreeFriend(Account account, String id_friend) {
		String sql ="UPDATE relationship  SET status = 1, date_update = now() WHERE id_user = ? and id_friend = ?";
		Connection cn = DBHelper.returnConnection();
		
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_friend );
			prepare.executeUpdate();
			prepare= cn.prepareCall(sql);
			prepare.setString(1, id_friend );
			prepare.setString(2, id_user );
			prepare.executeUpdate();
			return true;
		}catch(Exception e){
			System.out.print("loi add friend");
			return false;
		}
	}

	@Override
	public boolean deleteFriend(Account account, String id_friend) {
		String sql ="DELETE FROM relationship WHERE id_user = ? and id_friend = ?";
		Connection cn = DBHelper.returnConnection();
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_friend );
			prepare.executeUpdate();
			prepare= cn.prepareCall(sql);
			prepare.setString(1, id_friend );
			prepare.setString(2, id_user );
			prepare.executeUpdate();
			return true;
		}catch(Exception e){
			System.out.print("loi add friend");
			return false;
		}
	}

	@Override
    public Account insertArticalOfUser(Account account, String content) {
        Article article = new Article();
        String sql = "CALL insert_post(?,?,?,?,?)";
        Connection cn = DBHelper.returnConnection();
        CallableStatement ps = null;
        try{
            ps = cn.prepareCall(sql);
            ps.setInt(1, account.getIdUser() );
            ps.setString(2, content);
            ps.registerOutParameter(3, Types.INTEGER);
            ps.registerOutParameter(4, Types.VARCHAR);
            ps.registerOutParameter(5, Types.DATE);
            ps.executeUpdate();
            article.setIdPost(ps.getInt(3));
            article.setContent(ps.getString(4));
            article.setDateCreate(ps.getString(5));
            article.setIdUser(account.getIdUser());
            article.setNameOfUser(account.getProfile().getName());
            article.setAvatar(account.getProfile().getAvatar());
            account.getArticles().add(article);
            return account;
        }catch(Exception e){
            return account;
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
    }

	@Override
	public Account selectPeopleMayKnow(Account account) {
		String sql = "SELECT id_user,name,dob,gender,phone,job,city,avatar FROM profile WHERE MATCH(city) AGAINST(?)";		
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement prepare = null;
		
		try {
			prepare = cn.prepareStatement(sql);
			prepare.setString(1, account.getProfile().getCity());
			rs = prepare.executeQuery();				
			while(rs.next() && account.getPeopleMayKnow().size() < 4){
				if(rs.getInt(1) == account.getIdUser()){
					
				}else{
				Profile profile = new Profile();
				profile.setId_user(rs.getInt(1));
				profile.setName(rs.getString(2));
				profile.setDob(rs.getString(3));
				profile.setGender(rs.getInt(4));
				profile.setPhone(rs.getString(5));
				profile.setJob(rs.getString(6));
				profile.setCity(rs.getString(7));
				profile.setAvatar(rs.getString(8));
				account.getPeopleMayKnow().add(profile);
				}
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (prepare != null) try { prepare.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}

	@Override
	public Account insertComment(Account account, String content, int idPost) {
		Comment comment = new Comment();
        String sql = "CALL insert_comment(?,?,?,?,?)";
        Connection cn = DBHelper.returnConnection();
        CallableStatement ps = null;
        try{
            ps = cn.prepareCall(sql);
            ps.setInt(1, account.getIdUser() );
            ps.setString(2, content);
            ps.setInt(3, idPost);
            ps.registerOutParameter(4, Types.INTEGER);
            ps.registerOutParameter(5, Types.DATE);
            ps.executeUpdate();
            comment.setIdComment(ps.getInt(4));
            comment.setContent(content);
            comment.setIdUser(account.getIdUser());
            comment.setIdPost(idPost);
            comment.setNameOfUser(account.getProfile().getName());
            comment.setDateCreate(ps.getString(5));
            comment.setAvatar(account.getProfile().getAvatar());
            for(int i = 0 ; i < account.getArticles().size();i++){
            	if(account.getArticles().get(i).getIdPost() == idPost){
            		 account.getArticles().get(i).getComments().add(comment);
            		 break;
            	}
            }
           
            return account;
        }catch(Exception e){
            return account;
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
	}

	@Override
	public Account selectComment(Account account) {
		String sql = "SELECT id_comment,id_user,id_post,content,date_create,id_parent FROM comment WHERE id_post = ? ORDER BY date_create DESC";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, account.getIdUser());
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				Comment comment = new Comment();
				comment.setIdComment(rs.getInt(1));
				comment.setIdUser(rs.getInt(2));
				comment.setIdPost(rs.getInt(3));
				comment.setContent(rs.getString(4));
				comment.setDateCreate(rs.getString(5));
				//comment.setIdParent(rs.getInt(columnIndex));
				
				for(int i = 0; i < account.getArticles().size();i++){
					if(account.getArticles().get(i).getIdPost() == rs.getInt(3)){
						comment.setAvatar(account.getProfile().getAvatar());
						comment.setNameOfUser(account.getProfile().getName());
						account.getArticles().get(i).getComments().add(comment);
						break;
					}
				}
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}

	@Override
	public List<Comment> selectCommentWithidPost(int idPost,int idUser) {
		String sql = "SELECT P.id_comment,P.id_user,P.id_post,P.content,P.date_create,P.id_parent,PF.avatar,PF.name FROM profile AS PF,comment AS P WHERE P.id_post = ? AND PF.id_user= ? ORDER BY date_create DESC";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<Comment> comments = new ArrayList<Comment>();
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, idPost);
			ps.setInt(2, idUser);
			
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				Comment comment = new Comment();
				comment.setIdComment(rs.getInt(1));
				comment.setIdUser(rs.getInt(2));
				comment.setIdPost(rs.getInt(3));
				comment.setContent(rs.getString(4));
				comment.setDateCreate(rs.getString(5));
				comment.setAvatar(rs.getString(7));
				comment.setNameOfUser(rs.getString(8));
				
				//comment.setIdParent(rs.getInt(columnIndex));
				
				comments.add(comment);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return comments;
	}	
	
    @Override
    public boolean  insertLike(int id_user, int id_post){
        String sql = "INSERT INTO like_post VALUES ( default,?, ?)";
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        try{
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id_user);
            ps.setInt(2, id_post);
            int result = ps.executeUpdate();
            if( result == 1){
                return true;
            }else {
                
                return false;
            }
        }catch(Exception e){
            System.out.println("Error function like post ");
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
        return false;
    }

    @Override
    public int selectNumberOfLike(int id_user, int id_post) {
        String sql = "SELECT COUNT(id_like) FROM like_post WHERE id_post = ? AND id_user = ?";
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int result = 0;
        try {
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id_post);
            ps.setInt(2, id_user);
            rs = ps.executeQuery();                       
            
            if(rs.next()){
               result = rs.getInt(1);
                
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
            return result;
    }

    @Override
    public boolean deleteLike(int id_user, int id_post) {
        String sql = "DELETE FROM like_post WHERE id_post = ? AND id_user = ? ";
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        
        try{
            
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id_post);
            ps.setInt(2, id_user);
            
            int result = ps.executeUpdate();
            
            if( result == 1){
                return true;
                
            }else {
                
                return false;
            }
        
        }catch(Exception e){
            System.out.println("Error delete like ");
        
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
            
        }
        
        return false;
    }

	

}
