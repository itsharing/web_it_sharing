package org.pnv.model;

public class Comment {
	private int idComment;
	private int idUser;
	private int idPost;
	private String content;
	private String dateCreate;
	private int idParent;
	private String nameOfUser;
	private String avatar;
	
	public Comment() {
		super();
	}

	public Comment(int idComment, int idUser, int idPost, String content, String dateCreate, int idParent,
			String nameOfUser, String avatar) {
		super();
		this.idComment = idComment;
		this.idUser = idUser;
		this.idPost = idPost;
		this.content = content;
		this.dateCreate = dateCreate;
		this.idParent = idParent;
		this.nameOfUser = nameOfUser;
		this.avatar = avatar;
	}

	public int getIdComment() {
		return idComment;
	}

	public void setIdComment(int idComment) {
		this.idComment = idComment;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}

	public int getIdParent() {
		return idParent;
	}

	public void setIdParent(int idParent) {
		this.idParent = idParent;
	}

	public String getNameOfUser() {
		return nameOfUser;
	}

	public void setNameOfUser(String nameOfUser) {
		this.nameOfUser = nameOfUser;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
}
