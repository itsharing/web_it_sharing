package org.pnv.model;

import java.util.ArrayList;
import java.util.List;

public class Article {
	private int idPost;
	private int idUser;
	private String content;
	private String dateCreate;
	private String dateUpdate;
	private String nameOfUser;
	private String avatar;
	private List<Comment> comments = new ArrayList<>();
    private int numberOfLike;

	
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Article() {
		super();
	}

	public Article(int idPost, int idUser, String content, String dateCreate, String dateUpdate, String nameOfUser, String avatar) {
		super();
		this.idPost = idPost;
		this.idUser = idUser;
		this.content = content;
		this.dateCreate = dateCreate;
		this.dateUpdate = dateUpdate;
		this.nameOfUser = nameOfUser;
		this.avatar = avatar;
	}
    public Article(int idPost, int idUser, String content, String dateCreate, String dateUpdate, String nameOfUser, String avatar, int numberOfLike) {
        super();
        this.idPost = idPost;
        this.idUser = idUser;
        this.dateUpdate = dateUpdate;
        this.nameOfUser = nameOfUser;
        this.avatar = avatar;
        this.numberOfLike =numberOfLike;

    }
    
    public int getNumberOfLike() {
        return numberOfLike;
    }

    public void setNumberOfLike(int numberOfLike) {
        this.numberOfLike = numberOfLike;
    }


	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}

	public String getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public String getNameOfUser() {
		return nameOfUser;
	}

	public void setNameOfUser(String nameOfUser) {
		this.nameOfUser = nameOfUser;
	}
	
}
