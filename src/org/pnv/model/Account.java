package org.pnv.model;

import java.util.ArrayList;
import java.util.List;

public class Account {
	private int idUser;
	private String email;
	private String password;
	private int isActive;
	private String verifyCode;
	private String dateCreate;
	private String dateUpdate;
	private Profile profile = new Profile();
	private List <RelationShip> relationShip = new ArrayList<RelationShip>();
	private List<Article> articles = new ArrayList<Article>();
	private List<Profile> friendList = new ArrayList<Profile>();
	private List<Profile> peopleMayKnow = new ArrayList<Profile>();
	private List<Profile> requesOfFriend = new ArrayList<Profile>();
	private List<Comment> comments = new ArrayList<Comment>();
	
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Account(int idUser, String email, String password, String verifyCode, String dateCreate) {
		super();
		this.idUser = idUser;
		this.email = email;
		this.password = password;
		this.isActive = 0;
		this.verifyCode = verifyCode;
		this.dateCreate = dateCreate;
	}
		
	public Account() {
		super();
	}
	
	public Account (AccountRequest accountRequest){
		this.idUser = accountRequest.getId();
		this.email = accountRequest.getEmail();
		this.password = accountRequest.getPassword();
		this.profile.setName(accountRequest.getProfile().getName());
		this.profile.setPhone(accountRequest.getProfile().getPhone());
		this.profile.setAvatar(accountRequest.getProfile().getAvatar());
		this.profile.setDob(accountRequest.getProfile().getDob());
		this.profile.setGender(accountRequest.getProfile().getGender());
		this.profile.setCity(accountRequest.getProfile().getCity());
		this.profile.setJob(accountRequest.getProfile().getJob());
	}
	
	
	
	

	public List<RelationShip> getRelationShip() {
		return relationShip;
	}

	public void setRelationShip(List<RelationShip> relationShip) {
		this.relationShip = relationShip;
	}

	public List<Profile> getRequesOfFriend() {
		return requesOfFriend;
	}

	public List<Profile> getPeopleMayKnow() {
		return peopleMayKnow;
	}

	public void setPeopleMayKnow(List<Profile> peopleMayKnow) {
		this.peopleMayKnow = peopleMayKnow;
	}

	public List<Profile> getFriendList() {
		return friendList;
	}

	public void setFriendList(List<Profile> friendList) {
		this.friendList = friendList;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public String getVerifyCode() {
		return verifyCode;
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
	public String getDateCreate() {
		return dateCreate;
	}
	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}
	public String getDateUpdate() {
		return dateUpdate;
	}
	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public void setRequesOfFriend(List<Profile> requesOfFriend) {
		this.requesOfFriend = requesOfFriend;
	}
	
	public int getSize(){
		return friendList.size();
	}

	
	
	
}
