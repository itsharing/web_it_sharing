package org.pnv.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.pnv.business.AccountBoImpl;

/**
 * Servlet implementation class CheckEmailController
 */
//@WebServlet("/CheckEmailController")
public class CheckEmailController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckEmailController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		AccountBoImpl accountBoImpl = new AccountBoImpl();
		if(accountBoImpl.isExistEmail(request.getParameter("email")) == true){
			//response.getWriter().write("<img src=\"./assets/images/not-available.png\" />");
			response.getWriter().write("<div style='color:red'>This email has been used !</div>");
		}else{
			//response.getWriter().write("<img src=\"./assets/images/available.png\" />");
			response.getWriter().write("<div style='color:green'>You can use this email !</div>");
		}
	}

}
