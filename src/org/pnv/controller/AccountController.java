package org.pnv.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.pnv.business.AccountBo;
import org.pnv.business.AccountBoImpl;
import org.pnv.model.Account;
import org.pnv.model.AccountRequest;
import org.pnv.model.SearchResult;



/**
 * Servlet implementation class AccountController
 */
// @WebServlet("/AccountController")
public class AccountController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AccountBo accountBoImpl;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AccountController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String command = request.getParameter("command");
		if("changeProfile".equals(command)){
			RequestDispatcher view = request.getRequestDispatcher("jsp/profile.jsp");
			view.forward(request, response);
		}else if("register".equals(command)){
			RequestDispatcher view = request.getRequestDispatcher("jsp/register_new.jsp");
			view.forward(request, response);
		}else if("viewProfile".equals(command)){
			RequestDispatcher view = request.getRequestDispatcher("jsp/viewProfile.jsp");
			view.forward(request, response);
		}else if ("logout".equals(command)){
			HttpSession session = request.getSession();
			session.invalidate();
			request.getRequestDispatcher("jsp/register_new.jsp").forward(request, response);
		}else if (command.equals("anotherUser")){
			accountBoImpl = new AccountBoImpl();
			int id = Integer.parseInt(request.getParameter("id"));
			Account account = accountBoImpl.getAccount(id);
			request.setAttribute("anotherAccount", account);
			request.getRequestDispatcher("jsp/User_Page.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("utf-8");
		
		String command = request.getParameter("command");
		
		
		
		if (command.equals("login")) {
			String email = request.getParameter("form-username");
			String password = request.getParameter("form-password");
			
			accountBoImpl = new AccountBoImpl();
			int result = accountBoImpl.checkAccountExist(email, password);
			if (result == 1 ) {
				HttpSession session = request.getSession(false);
				session.setAttribute("account", accountBoImpl.getAccount(email, password));
				request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
			}else if(result == 0){
				request.setAttribute("email", email);
				request.setAttribute("password", password);
				request.getRequestDispatcher("jsp/verify_code.jsp").forward(request, response);
			} else{
				request.setAttribute("error", "wrong user or password");
				request.getRequestDispatcher("jsp/register_new.jsp").forward(request, response);
			}
		} else if(command.equals("register")){
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String name = request.getParameter("name");
			int gender = Integer.parseInt(request.getParameter("gender"));
			String dob = request.getParameter("dob");
			String job = request.getParameter("job");
			String city = request.getParameter("city");
			
			AccountRequest accountRequest = new AccountRequest(email,password,name,dob,gender,job,city);
			
			accountBoImpl = new AccountBoImpl();
			boolean result = accountBoImpl.registerAccount(accountRequest);
			if(result == true){
				//Account account = new Account(accountRequest);
				request.setAttribute("email", email);
				request.setAttribute("password", password);
				request.getRequestDispatcher("jsp/verify_code.jsp").forward(request, response);
			}else{
				request.getRequestDispatcher("jsp/register_new.jsp").forward(request, response);
			}
		} else if(command.equals("verifyCode")){
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String verifyCode = request.getParameter("verifyCode");
			if(accountBoImpl.checkVerifyCode(verifyCode, email) == true){
				HttpSession session = request.getSession(false);
				session.setAttribute("account", accountBoImpl.getAccount(email, password));
				request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
			}else{
				request.setAttribute("email", email);
				request.setAttribute("password", password);
				request.setAttribute("error", "wrong verify code");
				request.getRequestDispatcher("jsp/verify_code.jsp").forward(request, response);
			}
		} else if(command.equals("changeProfile")){
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			String name = request.getParameter("name");
			int gender = Integer.parseInt(request.getParameter("gender"));
			String dob = request.getParameter("dob");
			String job = request.getParameter("job");
			String city = request.getParameter("city");
			String phone = request.getParameter("phone");
				
			accountBoImpl = new AccountBoImpl();
			account = accountBoImpl.changeProfile(account,name,gender,dob,job,city,phone);
			session.removeAttribute("account");
			session.setAttribute("account", account);
			request.setAttribute("note", "Change profile successfully");
			request.getRequestDispatcher("jsp/profile.jsp").forward(request, response);
			
		}else if(command.equals("changePassword")){
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			String oldPassword = request.getParameter("oldPassword");
			String newPassword = request.getParameter("newPassword");
			String reenter_newPassword = request.getParameter("confirmPassword");
			accountBoImpl = new AccountBoImpl();
			boolean checkPassword = accountBoImpl.changePassword(account, oldPassword, newPassword, reenter_newPassword);
			if( !checkPassword){
				request.setAttribute("note", "Can not change your password, your password wrong");
				request.getRequestDispatcher("jsp/profile.jsp").forward(request, response);
			}else{
				request.setAttribute("note", "Change password successfully");
				request.getRequestDispatcher("jsp/profile.jsp").forward(request, response);
			}
			
			
		}else if(command.equals("changeImage")){
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			String imageName = request.getParameter("avatar");
			account = accountBoImpl.changeProfilePicture(account, imageName);
			session.removeAttribute("account");
			session.setAttribute("account", account);
			request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
		}
		else if(command.equals("search")){
			String input = request.getParameter("data-search"); // name of type input is data-search
			accountBoImpl = new AccountBoImpl();
			//SearchResult searchResult = new SearchResult();
			List<SearchResult> listUser = accountBoImpl.searchUsers(input);			
			if( listUser.isEmpty() ){
				request.setAttribute("searchFail", "There is no result matches your search");
				request.getRequestDispatcher("jsp/info_searching_page.jsp").forward(request, response);
			}else{
				request.setAttribute("searchSuccess", "There are " + listUser.size() + " results match your search");
				request.setAttribute("list", listUser);				
				request.getRequestDispatcher("jsp/info_searching_page.jsp").forward(request, response);
			}						
		}else if(command.equals("addFriend")){
			String addFriend = request.getParameter("addAFriend");
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			AccountBoImpl accountBoImpl  = new AccountBoImpl();
			if(accountBoImpl.addFriend(account, addFriend)){
				request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
			}else{
				request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
			}
		}else if(command.equals("agreeFriend")){
			String agreeFriend = request.getParameter("agreeAFriend");
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			AccountBoImpl accountBoImpl  = new AccountBoImpl();
			if(accountBoImpl.agreeFriend(account, agreeFriend)){
				request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
			}else{
				request.getRequestDispatcher("jsp/viewProfile.jsp");
			}
		}
		else if(command.equals("deleteFriend")){
			String deleteAFriend = request.getParameter("deleteAFriend");
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			AccountBoImpl accountBoImpl  = new AccountBoImpl();
			if(accountBoImpl.deleteFriend(account, deleteAFriend)){
				request.getRequestDispatcher("jsp/home_page.jsp").forward(request, response);
			}else{
				
				request.getRequestDispatcher("jsp/viewProfile.jsp");
			}
		}
		
	}
	/**
     * Extracts file name from HTTP header content-disposition
     */
}






