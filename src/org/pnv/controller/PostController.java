package org.pnv.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;

import org.pnv.business.AccountBo;
import org.pnv.business.AccountBoImpl;
import org.pnv.model.Account;

import com.google.gson.Gson;

/**
 * Servlet implementation class PostController
 */
@WebServlet("/PostController")
public class PostController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AccountBo accountBoImpl;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        accountBoImpl = new AccountBoImpl();
        String command = request.getParameter("command");
        int id_post = Integer.parseInt(request.getParameter("idPost"));
        if(command.equals("likePost")){
             HttpSession session = request.getSession();
             Account account = (Account) session.getAttribute("account");
             
             account = accountBoImpl.Like(account, id_post);
             session.removeAttribute("account");
             session.setAttribute("account", account);
             for(int i = 0; i < account.getArticles().size();i++){
                 if(account.getArticles().get(i).getIdPost() == id_post){
                     response.setContentType("text");
                     response.getWriter().write(""+account.getArticles().get(i).getNumberOfLike());
                  break;
                }
             }
             
         }else if(command.equals("unlikePost")){
             HttpSession session = request.getSession();
             Account account = (Account) session.getAttribute("account");
             account = accountBoImpl.UnLike(account, id_post);
             session.removeAttribute("account");
             session.setAttribute("account", account);
             for(int i = 0; i < account.getArticles().size();i++){
                 if(account.getArticles().get(i).getIdPost() == id_post){
                     response.setContentType("text");
                     response.getWriter().write("" + account.getArticles().get(i).getNumberOfLike());
                     break;
                 }
             }
         }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String command = request.getParameter("command");
		
		if(command.equals("post")){
			accountBoImpl = new AccountBoImpl();
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			String input = request.getParameter("input");
			account = accountBoImpl.Article(account, input);
			session.removeAttribute("account");
		    session.setAttribute("account", account);
		    request.setAttribute("newArticle", account.getArticles().get(account.getArticles().size()-1));
			final StringWriter buffer = new StringWriter();
            request.getRequestDispatcher("jsp/Post_Form.jsp").include(request, new HttpServletResponseWrapper(response) {
                private PrintWriter writer = new PrintWriter(buffer);

                @Override
                public PrintWriter getWriter() throws IOException {
                    return writer;
                }
            });

            String html = buffer.toString();
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("html", html);
			response.setContentType("application/json");  
			response.setCharacterEncoding("UTF-8"); 
			response.getWriter().write(new Gson().toJson(data));  
			    
			
			
		}else if(command.equals("comment")){
			accountBoImpl = new AccountBoImpl();
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("account");
			String content = request.getParameter("input");
			int idPost = Integer.parseInt(request.getParameter("idPost"));
			account = accountBoImpl.addComment(account, content, idPost);
			session.removeAttribute("account");
		    session.setAttribute("account", account);
		    int noOfPost = 0;
		    for(int i = 0; i < account.getArticles().size(); i++){
		    	if(account.getArticles().get(i).getIdPost() == idPost){
		    		noOfPost = i;
		    		break;
		    	}
		    	
		    }
		    int noOfComment = account.getArticles().get(noOfPost).getComments().size() - 1;
		    request.setAttribute("newComment", account.getArticles().get(noOfPost).getComments().get(noOfComment));
			final StringWriter buffer = new StringWriter();
            request.getRequestDispatcher("jsp/Comment_Form.jsp").include(request, new HttpServletResponseWrapper(response) {
                private PrintWriter writer = new PrintWriter(buffer);

                @Override
                public PrintWriter getWriter() throws IOException {
                    return writer;
                }
            });

            String html = buffer.toString();
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("html", html);
			response.setContentType("application/json");  
			response.setCharacterEncoding("UTF-8"); 
			response.getWriter().write(new Gson().toJson(data));  
		}else{
			response.setContentType("text/plain");  
		    response.setCharacterEncoding("UTF-8"); 
		    response.getWriter().write("khong dang duocj");  
		}
	}

}
