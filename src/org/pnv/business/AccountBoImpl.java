package org.pnv.business;

import java.util.List;
import org.pnv.dao.AccountDaoImpl;
import org.pnv.model.Account;
import org.pnv.model.AccountRequest;
import org.pnv.model.SearchResult;
import org.pnv.util.Utils;

public class AccountBoImpl implements AccountBo {

	AccountDaoImpl accountDaoImpl;

	@Override
	public int checkAccountExist(String email, String password) { // Check
																	// account
																	// service ?
		accountDaoImpl = new AccountDaoImpl();
		return accountDaoImpl.selectDataToCheckLogin(email, password);
	}

	@Override
	public boolean registerAccount(AccountRequest accountRequest) {
		accountDaoImpl = new AccountDaoImpl();
		String code = Utils.verifyEmailToActivedAccount(accountRequest.getEmail(),
				accountRequest.getProfile().getName());
		Account account = new Account(accountRequest);
		account.setVerifyCode(code);
		return accountDaoImpl.insertNewAccount(account);
	}

	@Override
	public boolean checkVerifyCode(String verifyCode, String email) {
		accountDaoImpl = new AccountDaoImpl();
		if (verifyCode.equals(accountDaoImpl.selectCode(email))) {
			return accountDaoImpl.updateStatus(email);
		}
		return false;
	}

	@Override
	public boolean isExistEmail(String email) {
		accountDaoImpl = new AccountDaoImpl();
		return accountDaoImpl.selectEmail(email);

	}

	@Override
	public Account getAccount(String email, String password) {
		accountDaoImpl = new AccountDaoImpl();
		Account account = new Account();
		account = accountDaoImpl.selectAccount(email, password);
		account = accountDaoImpl.selectFriendOfUser(account);
		account = accountDaoImpl.selectFriendsRequest(account);
		account = accountDaoImpl.selectStatusFriend(account);
		account = accountDaoImpl.selectProfile(account);
		account = accountDaoImpl.selectPeopleMayKnow(account);
		
		account = setArticlesForAccount(account);
		//account = accountDaoImpl.selectComment(account);
		return account;
	}
	
	@Override
	public Account getAccount(int idUser) {
		accountDaoImpl = new AccountDaoImpl();
		Account account = new Account();
		account = accountDaoImpl.selectAccount(idUser);
		account = accountDaoImpl.selectFriendOfUser(account);
		account = accountDaoImpl.selectFriendsRequest(account);
		account = accountDaoImpl.selectStatusFriend(account);
		account = accountDaoImpl.selectProfile(account);
		account = accountDaoImpl.selectPeopleMayKnow(account);
		
		account = setArticlesForAccount(account);
		
		//account = accountDaoImpl.selectComment(account);
		return account;
	}

	@Override
	public Account changeProfile(AccountRequest accountRequest) {
		accountDaoImpl = new AccountDaoImpl();
		Account account = new Account(accountRequest);
		boolean check = accountDaoImpl.isUpdateInformationSuccessfull(account);
		if (check == true) {
			account = accountDaoImpl.selectProfile(account);
			return account;
		} else {
			return null;
		}
	}
	
	@Override
	public Account changeProfile(Account account, String name, int gender, String dob, String job, String city, String phone) {
		accountDaoImpl = new AccountDaoImpl();
		boolean check = accountDaoImpl.isUpdateInformationSuccessfull(account,  name,  gender,  dob,  job,  city,  phone);
		if (check == true) {
			account = accountDaoImpl.selectProfile(account);
			return account;
		} else {
			return null;
		}
	}

	@Override
	public boolean changePassword(Account account, String oldPassword, String newPassword, String reenter_newPassword) {
		accountDaoImpl = new AccountDaoImpl();
		if (newPassword.equals(reenter_newPassword)) {
			boolean check = accountDaoImpl.isUpdatePasswordSuccessful(account.getEmail(), oldPassword, newPassword);
			if (check == true) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public Account changeProfilePicture(Account account, String ULR_Avatar) {
		accountDaoImpl = new AccountDaoImpl();
		if (accountDaoImpl.isUpdateAvatarSuccessful(ULR_Avatar, account.getIdUser())) {
			account.getProfile().setAvatar(accountDaoImpl.selectAvatar(account.getIdUser()));
			return account;
		} else {
			return account;
		}
	}

	@Override
	public Account setArticlesForAccount(Account account) {
		accountDaoImpl = new AccountDaoImpl();
		return accountDaoImpl.selecetArticleWithArticle(account);
	}

	@Override
	public Account postArticle(int idUser, String content) {
		accountDaoImpl = new AccountDaoImpl();
		Account account = null;
		if(accountDaoImpl.insertArticalOfUser(idUser, content) == true){
			account = getAccount(idUser);
		}
		return account;
	}

	@Override
	public List<SearchResult> searchUsers(String name) {
		accountDaoImpl = new AccountDaoImpl();
		String data_search = Utils.normalizationDataSearch(name);
		List<SearchResult> listUser = accountDaoImpl.search(data_search);
		return listUser;
	}

	@Override
	public boolean addFriend(Account account, String addFriend) {
		AccountDaoImpl accountDaoImpl =  new AccountDaoImpl();
		if(accountDaoImpl.addFriend(account, addFriend)){
			accountDaoImpl.selectFriendOfUser(account);
			accountDaoImpl.selectFriendsRequest(account);
			accountDaoImpl.selectStatusFriend(account);
			return true;
		}
		return false;
	}
	@Override
	public boolean deleteFriend(Account account, String addFriend) {
		AccountDaoImpl accountDaoImpl =  new AccountDaoImpl();
		if(accountDaoImpl.deleteFriend(account, addFriend)){
			accountDaoImpl.selectFriendOfUser(account);
			accountDaoImpl.selectFriendsRequest(account);
			accountDaoImpl.selectStatusFriend(account);
			return true;
		}
		return false;
	}
	@Override
	public boolean agreeFriend(Account account, String addFriend) {
		AccountDaoImpl accountDaoImpl =  new AccountDaoImpl();
		if(accountDaoImpl.agreeFriend(account, addFriend)){
			accountDaoImpl.selectFriendOfUser(account);
			accountDaoImpl.selectFriendsRequest(account);
			accountDaoImpl.selectStatusFriend(account);
			return true;
		}
		return false;
	}

	@Override
	public Account Article(Account account, String postContent) {
        accountDaoImpl = new AccountDaoImpl();
        return accountDaoImpl.insertArticalOfUser(account, postContent);

	}

	@Override
	public Account addComment(Account account, String content, int idPost) {
		accountDaoImpl = new AccountDaoImpl();
        return accountDaoImpl.insertComment(account, content,idPost);
	}
	
    public Account Like(Account account, int id_post) {
        // TODO Auto-generated method stub
        accountDaoImpl = new AccountDaoImpl();
        if(accountDaoImpl.insertLike(account.getIdUser(), id_post) == true){
            for(int i = 0; i < account.getArticles().size();i++){
                if(account.getArticles().get(i).getIdPost() == id_post){
                    account.getArticles().get(i).setNumberOfLike(accountDaoImpl.selectNumberOfLike(account.getIdUser(), id_post));
                }
            }
        }
        
        return account;

    }

    @Override
    public Account UnLike(Account account, int id_post) {
        accountDaoImpl = new AccountDaoImpl();
        if(accountDaoImpl.deleteLike(account.getIdUser(), id_post)== true){
            for(int i = 0; i < account.getArticles().size();i++){
                if(account.getArticles().get(i).getIdPost() == id_post){
                    account.getArticles().get(i).setNumberOfLike(accountDaoImpl.selectNumberOfLike(account.getIdUser(), id_post));
                }
            }
        }
        return account;
    }

	
}
