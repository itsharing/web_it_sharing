package org.pnv.util;

import java.sql.*;

public class DBHelper {

	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String URL = "jdbc:mysql://";
	private static final String HOST = "localhost"; //Change into "sql6.freesqldatabase.com" if you want to use online Database
	private static final String PORT = ":3306/"; //The default connection port
	private static final String DATABASE_NAME = "itsharing"; //Change into "sql6144335" if you want to use online Database
	private static final String ENCODING = "?useUnicode=true&characterEncoding=UTF-8";
	private static final String USER_NAME = "root"; //This is default user_name, change into "sql6144335" if you want to use online Database
	private static final String PASS_WORD = ""; //This is default pass_word, change into "eVKWIRpWfJ" if you want to use online Database
	private static Connection connect;
	
	public static Connection returnConnection() {
		try {
			Class.forName(DRIVER); //Declare driver for connection
			connect = DriverManager.getConnection(URL + HOST + PORT + DATABASE_NAME + ENCODING, USER_NAME, PASS_WORD); // Get connection
			if (connect != null) {
                System.out.println("Connected to the database!"); // Display if connect successfully
                return connect;
            }
        } catch (SQLException ex) {
        	System.out.println("Cannot connect to the database!"); // Display if connect unsuccessfully
        } catch (ClassNotFoundException ex) {
        	System.out.println("Cannot find the driver!"); // Display if there is no driver
		}
		return null;
    }
	
	public static void main(String args[]){
		
		System.out.println(DBHelper.returnConnection());
	}
}
