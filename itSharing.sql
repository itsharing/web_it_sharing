-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema itsharing
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema itsharing
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `itsharing` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `itsharing` ;

-- -----------------------------------------------------
-- Table `itsharing`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsharing`.`account` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `isActive` TINYINT(1) NOT NULL DEFAULT 0,
  `verify_code` VARCHAR(6) NULL,
  `date_create` DATETIME NOT NULL,
  `date_update` DATETIME NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


INSERT INTO account (id_user, email, password, isActive, verify_code, date_create) VALUES (1, 'an@gmail.com', MD5('123'), 1, '123456', NOW());
INSERT INTO account (id_user, email, password, isActive, verify_code, date_create) VALUES (2, 'sang@gmail.com', MD5('123'), 1, '123456', NOW());
INSERT INTO account (id_user, email, password, isActive, verify_code, date_create) VALUES (3, 'linh@gmail.com', MD5('123'), 1, '123456', NOW());
INSERT INTO account (id_user, email, password, isActive, verify_code, date_create) VALUES (4, 'thanh@gmail.com', MD5('123'), 1, '123456', NOW());
INSERT INTO account (id_user, email, password, isActive, verify_code, date_create) VALUES (5, 'cuong@gmail.com', MD5('123'), 1, '123456', NOW());


-- -----------------------------------------------------
-- Table `itsharing`.`profile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsharing`.`profile` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `dob` DATE NOT NULL,
  `gender` TINYINT(1) NOT NULL,
  `phone` VARCHAR(11) NULL,
  `job` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `city` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `avatar` VARCHAR(45) CHARACTER SET 'utf8' NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `fk_profile_account1`
    FOREIGN KEY (`id_user`)
    REFERENCES `itsharing`.`account` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `itsharing`.`profile`.FullText
-- -----------------------------------------------------
ALTER TABLE profile
ADD FULLTEXT(name);



INSERT INTO profile (id_user, name, dob, gender, job, city) VALUES (1, 'Trương Nguyễn Thiên Ân', '1996-01-09', 1, 'Developer', 'Đà Nẵng');
INSERT INTO profile (id_user, name, dob, gender, job, city) VALUES (2, 'Trần Minh Sang', '1996-01-09', 1, 'Developer', 'Đà Nẵng');
INSERT INTO profile (id_user, name, dob, gender, job, city) VALUES (3, 'Trần Thị Mỹ Linh', '1996-01-09', 1, 'Developer', 'Đà Nẵng');
INSERT INTO profile (id_user, name, dob, gender, job, city) VALUES (4, 'Nguyễn Thị Vân Thanh', '1996-01-09', 1, 'Developer', 'Đà Nẵng');
INSERT INTO profile (id_user, name, dob, gender, job, city) VALUES (5, 'Trần Xuân Cường', '1996-01-09', 1, 'Developer', 'Đà Nẵng');


-- -----------------------------------------------------
-- Table `itsharing`.`post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsharing`.`post` (
  `id_post` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `content` VARCHAR(1000) CHARACTER SET 'utf8' NOT NULL,
  `date_create` DATETIME NOT NULL,
  `date_update` DATETIME NULL,
  PRIMARY KEY (`id_post`),
  INDEX `fk_post_account1_idx` (`id_user` ASC),
  CONSTRAINT `fk_post_account1`
    FOREIGN KEY (`id_user`)
    REFERENCES `itsharing`.`account` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



INSERT INTO post (id_user, content, date_create) VALUES (1, 'Mình là Ân', NOW());
INSERT INTO post (id_user, content, date_create) VALUES (2, 'Mình là Sang', NOW());
INSERT INTO post (id_user, content, date_create) VALUES (3, 'Mình là Linh', NOW());
INSERT INTO post (id_user, content, date_create) VALUES (4, 'Mình là Thanh', NOW());
INSERT INTO post (id_user, content, date_create) VALUES (5, 'Cường là Chó', NOW());


-- -----------------------------------------------------
-- Table `itsharing`.`comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsharing`.`comment` (
  `id_comment` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `id_post` INT NOT NULL,
  `content` VARCHAR(300) CHARACTER SET 'utf8' NOT NULL,
  `date_create` DATETIME NOT NULL,
  `id_parent` INT NULL,
  PRIMARY KEY (`id_comment`),
  INDEX `fk_comment_account1_idx` (`id_user` ASC),
  INDEX `fk_comment_post1_idx` (`id_post` ASC),
  CONSTRAINT `fk_comment_account1`
    FOREIGN KEY (`id_user`)
    REFERENCES `itsharing`.`account` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_post1`
    FOREIGN KEY (`id_post`)
    REFERENCES `itsharing`.`post` (`id_post`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `itsharing`.`like_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsharing`.`like_comment` (
  `id_like` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `id_comment` INT NOT NULL,
  PRIMARY KEY (`id_like`),
  INDEX `fk_like_comment_account1_idx` (`id_user` ASC),
  INDEX `fk_like_comment_comment1_idx` (`id_comment` ASC),
  CONSTRAINT `fk_like_comment_account1`
    FOREIGN KEY (`id_user`)
    REFERENCES `itsharing`.`account` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_like_comment_comment1`
    FOREIGN KEY (`id_comment`)
    REFERENCES `itsharing`.`comment` (`id_comment`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `itsharing`.`like_post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsharing`.`like_post` (
  `id_like` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `id_post` INT NOT NULL,
  PRIMARY KEY (`id_like`),
  INDEX `fk_like_post_account1_idx` (`id_user` ASC),
  INDEX `fk_like_post_post1_idx` (`id_post` ASC),
  CONSTRAINT `fk_like_post_account1`
    FOREIGN KEY (`id_user`)
    REFERENCES `itsharing`.`account` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_like_post_post1`
    FOREIGN KEY (`id_post`)
    REFERENCES `itsharing`.`post` (`id_post`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `itsharing`.`relationship`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsharing`.`relationship` (
  `id_user` INT NOT NULL,
  `id_friend` INT NOT NULL,
  `status` INT NOT NULL,
  `date_create` DATETIME NOT NULL,
  `date_update` DATETIME NULL,
  PRIMARY KEY (`id_user`, `id_friend`),
  INDEX `fk_relationship_account1_idx` (`id_friend` ASC),
  CONSTRAINT `fk_relationship_account`
    FOREIGN KEY (`id_user`)
    REFERENCES `itsharing`.`account` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_relationship_account1`
    FOREIGN KEY (`id_friend`)
    REFERENCES `itsharing`.`account` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Procedure `insert_account`
-- -----------------------------------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS insert_account $$
CREATE PROCEDURE insert_account( IN email1 VARCHAR(100), IN password1 VARCHAR(100), IN verifycode1 VARCHAR(6), IN name1 VARCHAR(45) CHARSET UTF8,
                                IN dob1 DATE, IN gender1 INT, IN city1 VARCHAR(45) CHARSET UTF8, IN job1 VARCHAR(45) CHARSET UTF8, OUT result INT)
BEGIN
    DECLARE last_id INT DEFAULT 0;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        SET result = 0;
        ROLLBACK;
    END; 
    
    START TRANSACTION;
    
    INSERT INTO account(email, password, isActive, verify_code, date_create)
    VALUES(email1, password1, 0, verifycode1, NOW());
    
    SELECT id_user INTO last_id FROM account WHERE id_user = LAST_INSERT_ID();
    
    INSERT INTO profile(id_user, name,  dob, gender, city, job)
    VALUES(last_id, name1, dob1, gender1, city1, job1);
    
    SET result = 1;
    COMMIT;    
END $$
DELIMITER ;

-- -----------------------------------------------------
-- Procedure `insert_post`
-- -----------------------------------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS insert_post $$
CREATE PROCEDURE insert_post( in id int, in input_content varchar(225), out idPost int, out output_content varchar(225),out dateCreate DATE)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        
        ROLLBACK;
       
    END; 
    
    START TRANSACTION;
    
    INSERT INTO  post(id_user,content,date_create) VALUES(id,input_content,NOW());
    
    SELECT id_post,content,date_create INTO idPost, output_content,dateCreate FROM post WHERE id_post = LAST_INSERT_ID();
    
   
    COMMIT;
END $$
DELIMITER ;

-- -----------------------------------------------------
-- ADD fulltext for column city in profile table
-- -----------------------------------------------------
ALTER TABLE profile ADD FULLTEXT(city);

-- -----------------------------------------------------
-- Procedure `insert_comment`
-- -----------------------------------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS insert_comment $$
CREATE PROCEDURE insert_comment( in idUser int, in input_content varchar(225), in idPost int, out idComment int,out dateCreate DATE)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        
        ROLLBACK;
       
    END; 
    
    START TRANSACTION;
    
    INSERT INTO  comment(id_user, id_post, content, date_create) VALUES(idUser, idPost, input_content, NOW());
    
    SELECT id_comment, date_create INTO idComment, dateCreate FROM comment WHERE id_comment = LAST_INSERT_ID();
    
   
    COMMIT;
END $$
DELIMITER ;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
